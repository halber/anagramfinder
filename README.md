# Anagram Finder
A simple sample application to search anagrams in a text file.

## build

To build the program install apache maven and run  
```
mvn install.
```  
The application requires Java 8.  

## run

To run the program execute on of the following commands:
```
java -jar target/AnagramFinder-1.0.jar sample.txt  
java -jar target/AnagramFinder-1.0.jar -v=1 sample.txt  
java -jar target/AnagramFinder-1.0.jar -v=2 sample.txt
java -jar target/AnagramFinder-1.0.jar -v=3 sample.txt
```

The text file should use the system default encoding.  
With the parameter -v you can choose the implementation that is used.

## test
Some example test executions on my computer with the King James Version of the Bible:  
  
Version 1:  
Required time: 212,627,340ns.  
Required time: 213,509,770ns.  
Required time: 211,113,719ns.  
  
Version 2:  
Required time: 293,744,502ns.  
Required time: 273,586,881ns.  
Required time: 279,962,450ns.  
  
Version 3:  
Required time: 235,416,812ns.  
Required time: 217,925,795ns.  
Required time: 202,206,070ns.