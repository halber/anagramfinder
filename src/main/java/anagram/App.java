/*
 * Copyright 2016 Michael Halberstadt.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package anagram;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author @author <a href="mailto:michael.halberstadt@gmail.com">Michael Halberstadt</a>
 */
public class App {

  public static void main(String[] args) throws IOException {

    AnagramFinder anagramFinder = params(args);

    Path path = Paths.get(args[args.length - 1]);
    if (!Files.exists(path, LinkOption.NOFOLLOW_LINKS)) {
      System.out.println(args[args.length - 1] + ": File not found.");
      System.exit(1);
    }

    String content = new String(Files.readAllBytes(path));
    List<String> words = Arrays.asList(content.split("\\s"));

    long timeStart = System.nanoTime();
    anagramFinder.findAnagrams(words);
    long timeEnd = System.nanoTime();
    
    listAnagrams(anagramFinder.getAnagrams());
    System.out.println("");
    System.out.println("Required time: " + (timeEnd - timeStart) + "ns.");
  }

  private static AnagramFinder params(String[] args) {
    if (args.length == 0) {
      printUsageAndExit();
    } else if (args.length == 1) {
      return new anagram.v1.HashAnagramFinder();
    } else if (args.length == 2) {
      if (null != args[0]) {
        switch (args[0]) {
          case "-v=1":
            System.out.println("using HashAnagramFinder version 1.");
            return new anagram.v1.HashAnagramFinder();
          case "-v=2":
            System.out.println("using HashAnagramFinder version 2.");
            return new anagram.v2.HashAnagramFinder();
          case "-v=3":
            System.out.println("using ReducedCharsetHashAnagramFinder.");
            return new anagram.v3.ReducedCharsetHashAnagramFinder();
        }
      }
    }
    printUsageAndExit();
    throw new IllegalStateException();
  }

  private static void printUsageAndExit() {
    System.out.println("Usage: java -jar AnagramFinder-1.0.jar sample.txt");
    System.out.println("or to choose the implementation run: java -jar AnagramFinder-1.0.jar -v=1 sample.txt");
    System.out.println("or: java -jar AnagramFinder-1.0.jar -v=2 sample.txt");
    System.exit(1);
  }

  public static void listAnagrams(Collection<Collection<String>> anagrams) {
    for (Collection<String> anagramStrings : anagrams) {
      String result = anagramStrings.stream().collect(Collectors.joining(" "));
      System.out.println(result);
    }
  }

}
