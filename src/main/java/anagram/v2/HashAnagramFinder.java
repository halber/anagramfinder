/*
 * Copyright 2016 Michael Halberstadt.
 */
package anagram.v2;

import anagram.AnagramFinder;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;

/**
 * @author @author <a href="mailto:michael.halberstadt@gmail.com">Michael Halberstadt</a>
 */
public class HashAnagramFinder implements AnagramFinder {

  private final Map<Anagram, Anagram> searchAnagramMap = new HashMap();
  private final Map<Anagram, Collection<String>> anagramMap = new HashMap<>();

  @Override
  public void findAnagrams(Iterable<String> words) {
    for (String word : words) {
      if (word.length() < 2) {
        continue; // there is no anagram to eg. 'a'.
      }
      Anagram anagram = new Anagram(word.toLowerCase());

      Anagram previous = searchAnagramMap.put(anagram, anagram);
      if (previous != null) {
        if (!previous.getAnagram().equals(anagram.getAnagram())) {
          addToAnagramMap(previous);
        }
      }
    }

    // add the missing values from searchAnagramMap
    for (Anagram anagram : anagramMap.keySet()) {
      addToAnagramMap(searchAnagramMap.get(anagram));
    }
  }

  private void addToAnagramMap(Anagram keyAnagram) {
    Objects.requireNonNull(keyAnagram);
    Collection<String> anagrams = this.anagramMap.get(keyAnagram);
    if (anagrams == null) {
      anagrams = new HashSet<>();
    }
    anagrams.add(keyAnagram.getAnagram());
    this.anagramMap.put(keyAnagram, anagrams);
  }

  @Override
  public Collection<Collection<String>> getAnagrams() {
    return anagramMap.values();
  }
}
