/*
 * Copyright 2016 Michael Halberstadt.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package anagram.v2;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author @author <a href="mailto:michael.halberstadt@gmail.com">Michael Halberstadt</a>
 */
class Anagram {

  private final String anagram;

  public Anagram(String word) {
    Objects.requireNonNull(word);
    this.anagram = word;
  }

  @Override
  public String toString() {
    return anagram;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    for (char c : anagram.toCharArray()) {
      hash = (59 + c) * hash;
    }
    hash = 59 * hash + this.anagram.length();
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    
    final Anagram other = (Anagram) obj;
    if (anagram.length() != other.anagram.length()) {
      return false;
    }

    char[] thisChars = anagram.toCharArray();
    Arrays.sort(thisChars);

    char[] otherChars = other.anagram.toCharArray();
    Arrays.sort(otherChars);

    return Arrays.equals(thisChars, otherChars);
    }

  public String getAnagram() {
    return anagram;
  }
}
