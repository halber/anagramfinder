/*
 * Copyright 2016 Michael Halberstadt.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package anagram.v3;

import anagram.AnagramFinder;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author @author <a href="mailto:michael.halberstadt@gmail.com">Michael Halberstadt</a>
 */
public class ReducedCharsetHashAnagramFinder implements AnagramFinder {

  private static final Map<Character, Integer> reducedCharsetMap = Collections.<Character, Integer>unmodifiableMap(new HashMap<Character, Integer>() {
    {
      put('a', 2);
      put('ä', 3);
      put('b', 5);
      put('c', 7);
      put('d', 11);
      put('e', 13);
      put('f', 17);
      put('g', 19);
      put('h', 23);
      put('i', 29);
      put('j', 31);
      put('k', 37);
      put('l', 41);
      put('m', 43);
      put('n', 47);
      put('o', 53);
      put('ö', 59);
      put('p', 61);
      put('q', 67);
      put('r', 71);
      put('s', 73);
      put('t', 79);
      put('u', 83);
      put('ü', 89);
      put('v', 97);
      put('w', 101);
      put('x', 103);
      put('y', 107);
      put('z', 109);
    }
  });

  private final Map<Number, Collection<String>> anagramMap = new HashMap<>();

  @Override
  public void findAnagrams(Iterable<String> words) {
    for (String word : words) {
      if (word.length() < 2) {
        continue; // there is no anagram to eg. 'a'.
      }

      Number key = buildKey(word);
      Collection<String> values = getValues(key);
      values.add(word);
      anagramMap.put(key, values);
    }
  }

  private Number buildKey(String anagram) {
    if (anagram.length() < 10) {
      return calcLongKey(anagram);
    } else {
      BigInteger result = BigInteger.ONE;
      for (int i = 0; i < anagram.length(); i += 10) {
        int endIndex = i + 10 < anagram.length() ? i + 10 : anagram.length();
        result = result.multiply(BigInteger.valueOf(calcLongKey(anagram.substring(i, endIndex))));
      }

      return result;
    }
  }

  private long calcLongKey(String anagram) {
    // This implementation can overflow a long value for Strings longer than 9 characters.
    long result = 1L;
    for (char character : anagram.toLowerCase().toCharArray()) {
      Integer characterValue = reducedCharsetMap.get(character);
      // ignore unknown characters
      if (characterValue != null) {
        result *= characterValue;
        //  result = Math.multiplyExact(result, characterValue);
      }
    }
    return result;
  }

  private Collection<String> getValues(Number key) {
    Collection<String> values = anagramMap.get(key);
    if (values == null) {
      values = new HashSet<>(1);
    }
    return values;
  }

  @Override
  public Collection<Collection<String>> getAnagrams() {
    return anagramMap.values().stream()
            .filter(entry -> entry.size() > 1)
            .collect(Collectors.toList());
  }
}
