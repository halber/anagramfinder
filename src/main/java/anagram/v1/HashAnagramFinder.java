/*
 * Copyright 2016 Michael Halberstadt.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package anagram.v1;

import anagram.AnagramFinder;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author @author <a href="mailto:michael.halberstadt@gmail.com">Michael Halberstadt</a>
 */
public class HashAnagramFinder implements AnagramFinder {

  private final Map<String, Collection<String>> anagramMap = new HashMap<>();

  @Override
  public void findAnagrams(Iterable<String> words) {
    for (String word : words) {
      if (word.length() < 2) {
        continue; // there is no anagram to eg. 'a'.
      }

      String key = buildKey(word);
      Collection<String> values = getValues(key);
      values.add(word);
      anagramMap.put(key, values);
    }
  }

  private String buildKey(String anagram) {
    char[] chars = anagram.toLowerCase().toCharArray();
    Arrays.sort(chars);
    return new String(chars);
  }

  private Collection<String> getValues(String key) {
    Collection<String> values = anagramMap.get(key);
    if (values == null) {
      values = new HashSet<>(1);
    }
    return values;
  }

  @Override
  public Collection<Collection<String>> getAnagrams() {
    return anagramMap.values().stream()
            .filter(entry -> entry.size() > 1)
            .collect(Collectors.toList());
  }
}
