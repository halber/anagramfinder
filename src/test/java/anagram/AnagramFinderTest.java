/*
 * Copyright 2016 Michael Halberstadt.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package anagram;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import static org.junit.Assert.*;

/**
 * @author @author <a href="mailto:michael.halberstadt@gmail.com">Michael Halberstadt</a>
 */
public class AnagramFinderTest {
  
  public static void testFindAnagrams(AnagramFinder anagramFinder) {
    System.out.println("findAnagrams");

    String content = "trace\n"
            + "cater\n"
            + "use\n"
            + "crate\n"
            + "saw\n"
            + "fries\n"
            + "shall\n"
            + "react\n"
            + "run\n"
            + "way\n"
            + "who\n"
            + "scar\n"
            + "salt\n"
            + "surf\n"
            + "arcs\n"
            + "yaw\n"
            + "cars\n"
            + "bear\n"
            + "rose\n"
            + "fiber";

    List<String> words = Arrays.asList(content.split("\\s"));

    anagramFinder.findAnagrams(words);

    Collection<Collection<String>> anagrams = anagramFinder.getAnagrams();
    assertNotNull(anagrams);
    assertFalse(anagrams.isEmpty());

    for (Collection<String> anagramStrings : anagrams) {
      Collection<String> result = anagramStrings;
      switch (result.size()) {
        case 2:
          result.remove("way");
          result.remove("yaw");
          assertTrue(result.isEmpty());
          break;
        case 3:
          result.remove("scar");
          result.remove("arcs");
          result.remove("cars");
          assertTrue(result.isEmpty());
          break;
        case 4:
          result.remove("trace");
          result.remove("react");
          result.remove("cater");
          result.remove("crate");
          assertTrue(result.isEmpty());
          break;
        default:
          fail("findAnagrams failed.");
          break;
      }
    }
  }
}
