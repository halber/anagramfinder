/*
 * Copyright 2016 Michael Halberstadt.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package anagram.v2;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author @author <a href="mailto:michael.halberstadt@gmail.com">Michael Halberstadt</a>
 */
public class AnagramTest {
  
  private List<Anagram> anagramsAbc;
  private List<Anagram> anagramsRandom;
  
  @Before
  public void setUp() {
    anagramsAbc = new ArrayList<>();
    anagramsAbc.add(new Anagram("aabc"));
    anagramsAbc.add(new Anagram("baca"));
    anagramsAbc.add(new Anagram("caba"));
    anagramsAbc.add(new Anagram("acba"));
    anagramsAbc.add(new Anagram("cbaa"));
    anagramsAbc.add(new Anagram("baca"));
    anagramsAbc.add(new Anagram("baca"));
    
    anagramsRandom = new ArrayList<>();
    anagramsRandom.add(new Anagram("adf#"));
    anagramsRandom.add(new Anagram("bacb"));
    anagramsRandom.add(new Anagram("343"));
    anagramsRandom.add(new Anagram("loj"));
    anagramsRandom.add(new Anagram("äöp"));
    anagramsRandom.add(new Anagram("äaw"));
    anagramsRandom.add(new Anagram("6&7"));
  }

  @Test
  public void testHashCode() {
    System.out.println("hashCode");
    Anagram abcAnagram = new Anagram("aabc");
    for(Anagram anagram1 : anagramsAbc) {
      assertEquals(anagram1.hashCode(), abcAnagram.hashCode());
    }
    
    for(Anagram anagram2 : anagramsRandom) {
      assertNotEquals(anagram2.hashCode(), abcAnagram.hashCode());
    }
  }

  @Test
  public void testEquals() {
    System.out.println("testEquals");
    Anagram abcAnagram = new Anagram("abac");
    for(Anagram anagram1 : anagramsAbc) {
      assertEquals(anagram1, abcAnagram);
    }
    
    for(Anagram anagram2 : anagramsRandom) {
      assertNotEquals(anagram2, abcAnagram);
    }
  }  
}
