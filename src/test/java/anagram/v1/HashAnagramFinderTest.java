/*
 * Copyright 2016 Michael Halberstadt.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package anagram.v1;

import anagram.AnagramFinder;
import anagram.AnagramFinderTest;
import org.junit.Test;

/**
 * @author @author <a href="mailto:michael.halberstadt@gmail.com">Michael Halberstadt</a>
 */
public class HashAnagramFinderTest {
    
  @Test
  public void testHashAnagramFinder() {
    System.out.println("test version 1 HashAnagramFinder");
    AnagramFinder anagramFinder = new anagram.v1.HashAnagramFinder();
    AnagramFinderTest.testFindAnagrams(anagramFinder);
  }
  
}
